#!/bin/bash
sudo apt-get update
sudo apt-get upgrade
#git
sudo apt-get install git
#curl 
sudo apt-get install curl
#jq
sudo apt install jq
#docker
sudo apt install docker.io
#docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
#check version
sudo git --version
sudo curl --version
sudo jq --version
sudo docker --version
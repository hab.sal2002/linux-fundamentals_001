#!/bin/bash

HOST="https://saintrivers.tech"
REALM="ams"

TOKEN=$(curl -s --location --request POST "$HOST/auth/realms/$REALM/protocol/openid-connect/token" \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data-urlencode 'grant_type=client_credentials' \
  --data-urlencode 'client_id=microservice-client' \
  --data-urlencode 'client_secret=9Yyq8DilKmhZmFeVc4NXi5CLgkWfYImJ' \
  --data-urlencode 'scopes=code' | jq -r '.access_token')

  echo
  echo "a. get an access token from the oauth2 url "
  echo
  echo -e "\033[1;35m ${TOKEN} \033[0m"
  echo

  echo
  echo "b. guse that access token to access a protected url "
  echo
  accessToken=$(curl -s -H "Authorization: Bearer ${TOKEN}" https://ams.saintrivers.tech/private/ | jq
   )
    echo -e "\033[1;33m ${accessToken} \033[0m"
    echo
  
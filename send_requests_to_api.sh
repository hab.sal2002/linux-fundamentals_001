#!/bin/bash
#shebang



# create two categories 
echo " ====== create two categories ====== "
echo -n Enter Category1:
read category1
curl -X 'POST' \
  'https://ams.saintrivers.tech/api/v1/articles/categories' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "'${category1}'"
}' | jq 

echo -n Enter Category2:
read category2
curl -X 'POST' \
  'https://ams.saintrivers.tech/api/v1/articles/categories' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "'${category2}'"
}' | jq 

# create an article
echo " ====== create an article ====== "
#get category id to pass article's request
category_id=$(curl -X 'GET' \
  'https://ams.saintrivers.tech/api/v1/articles/categories' \
  -H 'accept: */*' | jq '.[0].id'| tr -d '"' )
echo ${category_id}
echo -n Enter Title: 
read title
echo -n Enter Body:
read body
echo -n Enter Author:
read author
curl -X 'POST' \
  'https://ams.saintrivers.tech/api/v1/articles' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d '{
  "content": {
    "title": "'${title}'",
    "body": "'${body}'"
  },
  "createdAt": "'" $(date +%Y-%m-%dT%H:%M:%S) "'",
  "author": "'${author}'",
  "categories": [
    "'${category_id}'"
  ]
}' | jq


#add comments
echo " ====== add two comments to the article ====== "
#get article id to comments
article_id=$(curl -s -X 'GET' \
  'https://ams.saintrivers.tech/api/v1/articles' \
  -H 'accept: */*' | jq '.[0].id'| tr -d '"' )
  echo $article_id

echo -n Enter Caption1 :
read caption1
echo -n Enter Commenter1 :
read commenter1
  curl -X 'POST' \
  'https://ams.saintrivers.tech/api/v1/articles/'${article_id}'/comments' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d '{
  "caption": "'${caption1}'",
  "commentedOn": "'" $(date +%Y-%m-%dT%H:%M:%S) "'",
  "commenter": "'${commenter1}'"
}' | jq

echo -n Enter Caption2 :
read caption2
echo -n Enter Commenter2 :
read commenter2
curl -X 'POST' \
  'https://ams.saintrivers.tech/api/v1/articles/'${article_id}'/comments' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d '{
  "caption": "'${caption2}'",
  "commentedOn": "'" $(date +%Y-%m-%dT%H:%M:%S) "'",
  "commenter": "'${commenter2}'"
}' | jq